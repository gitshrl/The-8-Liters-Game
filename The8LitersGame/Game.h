#pragma once
#include "Display.h"
#include "Button.h"
#include "Color.h"
#include "NormalGlass.h"
#include "MagicGlass.h"
#include "HotGlass.h"
#include "Player.h"
#include "Sound.h"

enum Help { rule, instruction };

class Game
{
public:
	Game();
	~Game();
	void start();

private:
	/* data members */
	ALLEGRO_DISPLAY *display;
	ALLEGRO_EVENT_QUEUE *eventQueue;
	ALLEGRO_EVENT event;
	ALLEGRO_TIMER *timer;
	ALLEGRO_COLOR buttonTextColor[5];

	/* BOOLEAN : MOSTLY FOR SWITCH DISPLAY*/
	bool done;
	bool top;
	bool bottom;
	bool ground;
	bool redraw;
	bool endtype;
	bool type;
	bool isLoad;
	bool gameOver;
	bool bit; // to redraw bitmap after restart
	bool key; // to activate keyboard keycode

	/* DATA MEMBERS */
	int result;
	int curX;
	int curY;
	int selectedIndex;
	int numberOfSavedPlayer;
	int topIndex;
	int botIndex;
	int topSelectedIndex;
	int botSelectedIndex;	
	float fps; // frame per seconds
	string playerName;

	/* STL VECTORS */
	vector<Glass*> glasses;
	vector<Glass*> glassList;
	vector<Player*> players;
	vector<Player*> playerList;
	vector<Player*> playerRank;

	/**/
	Glass *glassOnTop;
	Glass *glassOnBot;
	Player *playerSelected;

	/* MANAGE FILE */
	fstream playerFile;
	fstream glassFile;

	/* DISPLAY OBJECTS */
	Display mainMenu;
	Display newGame;
	Display gamePlay;
	Display loadGame; // has been change to load latest game
	Display leaderboard; // has been chage to latest winners
	Display leader; // has been change to latest player
	Display status;
	Display name;
	Display chance;
	Display finish;
	Display guide;

	/* BUTTON OBJECTS */
	Button menu;
	Button load;
	Button play;
	Button back;
	Button reset;
	Button glassTop[3];
	Button glassBot[3];
	Button glassSelectedTop;
	Button glassSelectedBot;
	Button repeat;
	Button tap; // pourer liquids
	Button save;
	Button home;
	Button restart;
	Button help;

	/* COLOR OBJECT */
	Color color;

	/* FORMAT OBJECT */
	Format text;

	/* SOUND OBJECT */
	Sound backsound;

	/* METHODS */
	int getNumberOfSavedPlayer(); // to count player has been saved on file

	void initAllegro();
	void initGlass();
	void initPlayer(string);
	void loadBitmap();
	void loadGlassInfo();
	void loadPlayerInfo();
	void loadGameInfo();
	void showLeaderboard();
	void resetGameHistory();
	void saveGameInfo();
	void mixingProcess();
	void temperatureEffect();
	void getOutGlasses();
	void switchGlassBitmap(int);
	void insertPlayerName();
	void playerScore(int, int);
	void gameStatus();
	void manualGame(Help);
	void switchDisplay(); // to switch display
	void destroy();
};

