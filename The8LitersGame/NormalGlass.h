#pragma once
#include "Glass.h"

class NormalGlass :
	public Glass
{
public:
	NormalGlass();
	~NormalGlass();

	NormalGlass(char, int, float, float);

private:
	void showGlassInfo(ALLEGRO_FONT*, ALLEGRO_COLOR, char, Player*);
	void saveGlassInfo();
};