#include "Button.h"



Button::Button()
{
	Button::active = true;
	Button::numberOfButton = 0;
}


Button::~Button()
{
}

void Button::draw(int ax, int ay)
{
	// set initial distance for first bitmap from top
	int temp = 85;

	for (int i = 0; i < numberOfButton; i++) {
		// set constant distances between bitmaps after first bitmap
		if (i > 0) ay = 40;

		temp += ay;
		y[i] = temp;
		x[i] = ax;

		al_draw_bitmap(button[i], x[i], y[i], NULL);
	}
}

void Button::detectButtonHover(int curX, int curY)
{
	for (int i = 0; i < numberOfButton; i++) {
		if ((curX >= x[i]) && (curX <= x[i] + width) && (curY >= y[i]) && (curY <= y[i] + height)) {
			al_draw_bitmap(hover, x[i], y[i], NULL);
			buttonHover[i] = true;
		}
		else {
			buttonHover[i] = false;
		}
	}
}

void Button::reserveNumberOfButton(int numberOfButton)
{
	this->numberOfButton = numberOfButton;
}

void Button::destroy()
{
	al_destroy_bitmap(hover);

	for (int i = 0; i < numberOfButton; i++) {
		al_destroy_bitmap(button[i]);
	}
}
