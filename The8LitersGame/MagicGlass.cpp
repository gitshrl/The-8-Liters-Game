#include "MagicGlass.h"



MagicGlass::MagicGlass()
{
}


MagicGlass::~MagicGlass()
{
}

MagicGlass::MagicGlass(char glassPosition, int glassCapacity, float waterVolume, float oilVolume)
{
	this->glassPosition = glassPosition;
	this->glassCapacity = glassCapacity;
	this->waterVolume = waterVolume;
	this->oilVolume = oilVolume;
}

void MagicGlass::showGlassInfo(ALLEGRO_FONT *font, ALLEGRO_COLOR color, char position, Player *player)
{
	string txt1 = text.decimalPrecisionPoint(4, waterVolume);
	string txt2 = text.decimalPrecisionPoint(4, oilVolume);

	if (position == 'G') {
		al_draw_text(font, color, 339, 435, ALLEGRO_ALIGN_LEFT, txt1.c_str());
		al_draw_text(font, color, 339, 407, ALLEGRO_ALIGN_LEFT, txt2.c_str());
	}
	else if (position == 'B') {
		al_draw_text(font, color, 403, 263, ALLEGRO_ALIGN_LEFT, txt1.c_str());
		al_draw_text(font, color, 403, 235, ALLEGRO_ALIGN_LEFT, txt2.c_str());
	}
	else if (position == 'T') {
		al_draw_text(font, color, 310, 190, ALLEGRO_ALIGN_LEFT, txt1.c_str());
		al_draw_text(font, color, 310, 162, ALLEGRO_ALIGN_LEFT, txt2.c_str());
	}
}

void MagicGlass::saveGlassInfo()
{
	glassFile.open("glasses-saved.txt", ios::out | ios::app);

	glassFile << MAGIC_GLASS_TYPE << "\n" << glassPosition << " " << glassCapacity << " " << waterVolume << " " << oilVolume << endl;

	glassFile.close();
}
