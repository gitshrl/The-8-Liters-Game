#pragma once
#include "Header.h"

class Display
{
public:
	Display();
	~Display();

	ALLEGRO_BITMAP *background;
	ALLEGRO_FONT *font;

	bool done;

	void draw();
	void destroy();
};