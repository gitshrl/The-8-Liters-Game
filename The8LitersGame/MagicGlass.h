#pragma once
#include "Glass.h"

class MagicGlass :
	public Glass
{
public:
	MagicGlass();
	~MagicGlass();

	MagicGlass(char, int, float, float);

private:
	void showGlassInfo(ALLEGRO_FONT*, ALLEGRO_COLOR, char, Player*);
	void saveGlassInfo();
};

