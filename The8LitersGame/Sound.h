#pragma once
#include "Header.h"

class Sound
{
public:
	Sound();
	~Sound();

	ALLEGRO_SAMPLE *main;
	ALLEGRO_SAMPLE *gamePlay;
	ALLEGRO_SAMPLE *failed;
	ALLEGRO_SAMPLE *solve;
	ALLEGRO_SAMPLE *click;
	ALLEGRO_SAMPLE *water;

	ALLEGRO_SAMPLE_INSTANCE *mainInstance;
	ALLEGRO_SAMPLE_INSTANCE *gamePlayInstance;
	ALLEGRO_SAMPLE_INSTANCE *failedInstance;
	ALLEGRO_SAMPLE_INSTANCE *solveInstance;
	ALLEGRO_SAMPLE_INSTANCE *clickInstance;
	ALLEGRO_SAMPLE_INSTANCE *waterInstance;

	void init();
	void play(ALLEGRO_SAMPLE_INSTANCE*);
	void stop(ALLEGRO_SAMPLE_INSTANCE*);

	void destroy();
};

