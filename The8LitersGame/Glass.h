#pragma once
#include "Player.h"
#include "Button.h"
#include "Format.h"

enum Status { putOnTop, putOnBottom };
enum Fluid { water, oil };

class Glass
{
public:
	Glass();
	~Glass();

	/* pure virtual function */
	virtual void showGlassInfo(ALLEGRO_FONT*, ALLEGRO_COLOR, char, Player*) = 0;
	virtual void saveGlassInfo() = 0;

	/* setter virtual methods */
	virtual void setNewPosition(Status, Button);
	virtual void getOutGlasses();

	virtual void setNewWaterVolume(float);
	virtual void setNewOilVolume(float);
	virtual void setNewVolume(Fluid);

	/* getter virtual methods */
	virtual char getGlassPosition();
	virtual int getGlassCapacity();

	virtual float getGlassTemperature(Player*);
	virtual float getWaterVolume();
	virtual float getOilVolume();
	virtual float getGlassCharge();
	virtual float getGlassFreeSpace();

protected:
	char glassPosition;
	int glassCapacity;
	float oilVolume;
	float waterVolume;

	Format text;

	fstream glassFile;
};

