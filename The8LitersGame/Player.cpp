#include "Player.h"



Player::Player()
{
}


Player::~Player()
{
}

Player::Player(string playerName, float playerScore, int playerChance)
{
	this->playerName = playerName;
	this->playerScore = playerScore;
	this->playerChance = playerChance;
}

string Player::getPlayerName()
{
	return playerName;
}

float Player::getPlayerScore()
{
	return playerScore;
}

int Player::getPlayerChance()
{
	return playerChance;
}

void Player::setNewChance(GameStatus status)
{
	switch (status) {
	case running:
		playerChance--;
		break;
	case restarted:
		// player chance set to default
		playerChance = INITIAL_PLAYER_CHANCE;
		break;
	}
}

void Player::setNewScore(PlayerStatus status)
{
	switch (status) {
	case doingRightStep:
		playerScore += 5;
		break;
	case doingWrongStep:
		playerScore -= 1;
		break;
	case solveTheGame:
		playerScore += 100;
		break;
	case restartTheGame:
		playerScore = 0;
		break;
	}
}

void Player::savePlayerInfo()
{
	playerFile.open("players-saved.txt", ios::out | ios::app);

	playerFile << playerName << "\n" << playerScore << " " << playerChance << endl;

	playerFile.close();
}
