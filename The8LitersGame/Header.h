#pragma once

/* LIBRARY */
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <iomanip>
#include <sstream>
#include <algorithm>
using namespace std;



/*ALLEGRO LIBRARY */
#include <allegro5/allegro.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>



/*PLAYER PROPERTIES*/
#define INITIAL_PLAYER_CHANCE 13
#define INITIAL_PLAYER_SCORE 0
#define DEFAULT_CHARGE_TO_WIN 8

#define NORMAL_GLASS_TYPE "Normal Glass"
#define NORMAL_GLASS_POSITION 'G'
#define NORMAL_GLASS_CAPACITY 16
#define NORMAL_GLASS_WATER_VOLUME 8.0
#define NORMAL_GLASS_OIL_VOLUME 8.0

#define MAGIC_GLASS_TYPE "Magic Glass"
#define MAGIC_GLASS_POSITION 'G'
#define MAGIC_GLASS_CAPACITY 11
#define MAGIC_GLASS_WATER_VOLUME 0.0
#define MAGIC_GLASS_OIL_VOLUME 0.0

#define HOT_GLASS_TYPE "Hot Glass"
#define HOT_GLASS_POSITION 'G'
#define HOT_GLASS_CAPACITY 6
#define HOT_GLASS_WATER_VOLUME 0.0
#define HOT_GLASS_OIL_VOLUME 0.0
#define HOT_GLASS_INITIAL_TEMPERATURE 25.0



/* COLOR */
#define BLACK_RGB_CODE al_map_rgb(0, 0, 0)
#define WHITE_RGB_CODE al_map_rgb(255, 255, 255)
#define RED_RGB_CODE al_map_rgb(255, 0, 0)
#define GREEN_RGB_CODE al_map_rgb(0, 255, 0)
#define BLUE_RGB_CODE al_map_rgb(0, 0, 255)
#define ORANGE_RGB_CODE al_map_rgb(255, 102, 0)



/* GAME PROPERTIES*/
#define GAME_TITLE "The8LitersGame"
#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480



/* BITMAP PATH */
#define MAINMENU_BACKGROUND_IMAGE "img/background.png"
#define NEWGAME_BACKGROUND_IMAGE "img/newgame-backround.png"
#define LOADGAME_BACKGROUND_IMAGE "img/loadgame-background.png"
#define LEADERBOARD_BACKGROUND_IMAGE "img/leaderboard-background.png"
#define GAMEPLAY_BACKGROUND_IMAGE "img/game-play-background.png"

#define BUTTON_SHAPE_IMAGE "img/button-shape.png"
#define PLAY_BUTTON_IMAGE "img/play-button.png"
#define BACK_BUTTON_IMAGE "img/back-button.png"
#define LEFT_ARROW_IMAGE "img/left-arrow.png"
#define RIGHT_ARROW_IMAGE "img/right-arrow.png"
#define REPEAT_BUTTON_IMAGE "img/repeat.png"
#define SAVE_BUTTON_IMAGE "img/save.png"
#define HOME_BUTTON_IMAGE "img/home.png"
#define TAP_BUTTON_IMAGE "img/tap.png"
#define LOAD_BUTTON_IMAGE "img/load-shape.png"
#define RESTART_BUTTON_IMAGE "img/restart.png"
#define HELP_BUTTON_IMAGE "img/help.png"

#define BUTTON_SHAPE_HOVER_IMAGE "img/button-hover-shape.png"
#define PLAY_BUTTON_HOVER_IMAGE "img/play-button-hover.png"
#define BACK_BUTTON_HOVER_IMAGE "img/back-button-hover.png"
#define LEFT_ARROW_HOVER_IMAGE "img/left-arrow-hover.png"
#define RIGHT_ARROW_HOVER_IMAGE "img/right-arrow-hover.png"
#define REPEAT_BUTTON_HOVER_IMAGE "img/repeat-hover.png"
#define SAVE_BUTTON_HOVER_IMAGE "img/save-hover.png"
#define HOME_BUTTON_HOVER_IMAGE "img/home-hover.png"
#define TAP_BUTTON_HOVER_IMAGE "img/tap-hover.png"
#define LOAD_BUTTON_HOVER_IMAGE "img/load-shape-hover.png"
#define RESTART_BUTTON_HOVER_IMAGE "img/restart-hover.png"
#define HELP_BUTTON_HOVER_IMAGE "img/help-hover.png"
#define GUIDE_BACKGROUND_IMAGE "img/guide-background.png"
#define FINISH_BACKGROUND_IMAGE "img/finish-background.png"



/* GLASS BITMAP PATH */
#define GLASS_1_1_IMAGE "img/glass-11.png"
#define GLASS_1_2_IMAGE "img/glass-12.png"
#define GLASS_1_3_IMAGE "img/glass-13.png"
#define GLASS_1_4_IMAGE "img/glass-14.png"
#define GLASS_1_5_IMAGE "img/glass-15.png"
#define GLASS_1_6_IMAGE "img/glass-16.png"
#define GLASS_1_HOVER_IMAGE "img/glass-1-hover.png"

#define GLASS_2_1_IMAGE "img/glass-21.png"
#define GLASS_2_2_IMAGE "img/glass-22.png"
#define GLASS_2_3_IMAGE "img/glass-23.png"
#define GLASS_2_4_IMAGE "img/glass-24.png"
#define GLASS_2_5_IMAGE "img/glass-25.png"
#define GLASS_2_6_IMAGE "img/glass-26.png"
#define GLASS_2_HOVER_IMAGE "img/glass-2-hover.png"

#define GLASS_3_1_IMAGE "img/glass-31.png"
#define GLASS_3_2_IMAGE "img/glass-32.png"
#define GLASS_3_3_IMAGE "img/glass-33.png"
#define GLASS_3_4_IMAGE "img/glass-34.png"
#define GLASS_3_5_IMAGE "img/glass-35.png"
#define GLASS_3_6_IMAGE "img/glass-36.png"
#define GLASS_3_HOVER_IMAGE "img/glass-3-hover.png"




/* FONT PATH */
#define DEFAULT_FONT_SIZE 20
#define DEFAULT_FONT_TYPE "font/ShareTech-Regular.ttf"
#define INSERT_TEXT_FONT_SIZE 14
#define INSERT_TEXT_FONT_TYPE "font/Ubuntu-Regular.ttf"
#define TOP_LEADER_FONT_SIZE 24
#define TOP_LEADER_FONT_TYPE "font/DoHyeon-Regular.ttf"
#define GAME_PLAY_FONT_SIZE 20
#define GAME_PLAY_FONT_TYPE "font/Stylish-Regular.ttf"
#define NAME_FONT_SIZE 18
#define CHANCE_FONT_SIZE 36
#define GUIDE_FONT_SIZE 14
#define NAME_FONT_TYPE "font/HammersmithOne-Regular.ttf"
#define GAME_STATUS_FONT_SIZE 48
#define GAME_STATUS_FONT_TYPE "font/PermanentMarker-Regular.ttf"




/* BUTTON NUMBER*/
#define NUMBER_BUTTON_MENU 5
#define NUMBER_BUTTON_PLAY 1
#define NUMBER_BUTTON_BACK 1
#define NUMBER_BUTTON_ARROW 1
#define NUMBER_GLASS_BUTTON 3



/* FLUID PROPERTIES */
#define WATER_BOILING_POINT 100.0
#define OIL_BOILING_POINT 180.0