#pragma once
#include "Header.h"

class Color
{
public:
	Color();
	~Color();

	ALLEGRO_COLOR black;
	ALLEGRO_COLOR white;
	ALLEGRO_COLOR red;
	ALLEGRO_COLOR green;
	ALLEGRO_COLOR blue;
	ALLEGRO_COLOR orange;

	void init();
};

