#pragma once
#include "Glass.h"

class HotGlass :
	public Glass
{
public:
	HotGlass();
	~HotGlass();

	HotGlass(char, int, float, float, float);

	float getGlassTemperature(Player*);

private:
	float glassTemperature;
	int counter;

	void showGlassInfo(ALLEGRO_FONT*, ALLEGRO_COLOR, char, Player*);
	void saveGlassInfo();
};

