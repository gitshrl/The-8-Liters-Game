#pragma once
#include "Header.h"

enum GameStatus { running, restarted };

enum PlayerStatus { doingRightStep, doingWrongStep, solveTheGame, restartTheGame };

class Player
{
public:
	Player();
	~Player();

	Player(string, float, int);

	/* getter methods */
	string getPlayerName();
	float getPlayerScore();
	int getPlayerChance();

	/* setter methods */
	void setNewChance(GameStatus);
	void setNewScore(PlayerStatus);

	/* another methods */
	void savePlayerInfo();

private:
	string playerName;
	float playerScore;
	int playerChance;

	fstream playerFile;
};