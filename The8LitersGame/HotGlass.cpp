#include "HotGlass.h"



HotGlass::HotGlass()
{
	HotGlass::counter = 0;
}

HotGlass::~HotGlass()
{
}

HotGlass::HotGlass(char glassPosition, int glassCapacity, float waterVolume, float oilVolume, float glassTemperature)
{
	this->glassPosition = glassPosition;
	this->glassCapacity = glassCapacity;
	this->waterVolume = waterVolume;
	this->oilVolume = oilVolume;
	this->glassTemperature = glassTemperature;
}

float HotGlass::getGlassTemperature(Player *player)
{
	if (getGlassCharge() == 0) {
		glassTemperature = 25.0;
		counter = player->getPlayerChance();
	}
	else {
		if (player->getPlayerChance() == counter - 1) {
			glassTemperature += glassTemperature * 0.20;
			counter--;
		}
	}

	return glassTemperature;
}

void HotGlass::showGlassInfo(ALLEGRO_FONT *font, ALLEGRO_COLOR color, char position, Player *player)
{
	string txt1 = text.decimalPrecisionPoint(4, waterVolume);
	string txt2 = text.decimalPrecisionPoint(4, oilVolume);
	string txt3 = text.decimalPrecisionPoint(4, getGlassTemperature(player)) + " C";

	if (position == 'G') {
		al_draw_text(font, color, 403, 437, ALLEGRO_ALIGN_LEFT, txt1.c_str());
		al_draw_text(font, color, 403, 413, ALLEGRO_ALIGN_LEFT, txt2.c_str());
		al_draw_text(font, color, 377, 387, ALLEGRO_ALIGN_LEFT, txt3.c_str());

		if (glassTemperature > 180.0) al_draw_text(font, color, 433, 417, ALLEGRO_ALIGN_LEFT, "**oil evaporates**");
		if (glassTemperature > 100.0) al_draw_text(font, color, 433, 437, ALLEGRO_ALIGN_LEFT, "**water evaporates**");
	}
	else if (position == 'B') {
		al_draw_text(font, color, 403, 266, ALLEGRO_ALIGN_LEFT, txt1.c_str());
		al_draw_text(font, color, 403, 242, ALLEGRO_ALIGN_LEFT, txt2.c_str());
		al_draw_text(font, color, 379, 215, ALLEGRO_ALIGN_LEFT, txt3.c_str());
		
		if (glassTemperature > 180.0) al_draw_text(font, color, 433, 246, ALLEGRO_ALIGN_LEFT, "**oil evaporates**");
		if (glassTemperature > 100.0) al_draw_text(font, color, 433, 266, ALLEGRO_ALIGN_LEFT, "**water evaporates**");		
	}
	else if (position == 'T') {
		al_draw_text(font, color, 310, 193, ALLEGRO_ALIGN_LEFT, txt1.c_str());
		al_draw_text(font, color, 310, 168, ALLEGRO_ALIGN_LEFT, txt2.c_str());
		al_draw_text(font, color, 286, 142, ALLEGRO_ALIGN_LEFT, txt3.c_str());
		
		if (glassTemperature > 180.0) al_draw_text(font, color, 343, 173, ALLEGRO_ALIGN_LEFT, "**oil evaporates**");
		if (glassTemperature > 100.0) al_draw_text(font, color, 343, 193, ALLEGRO_ALIGN_LEFT, "**water evaporates**");
	}
}

void HotGlass::saveGlassInfo()
{
	glassFile.open("glasses-saved.txt", ios::out | ios::app);

	glassFile << HOT_GLASS_TYPE << "\n" << glassPosition << " " << glassCapacity << " "
		<< waterVolume << " " << oilVolume << " " << glassTemperature << endl;

	glassFile.close();
}
