#include "Glass.h"



Glass::Glass()
{
}


Glass::~Glass()
{
}

void Glass::setNewPosition(Status status, Button button)
{
	switch (status)
	{
	case putOnTop:
		glassPosition = 'T';
		button.draw(280, (135 - button.height));
		break;
	case putOnBottom:
		glassPosition = 'B';
		button.draw(372, (208 - button.height));
		break;
	}
}

void Glass::getOutGlasses()
{
	glassPosition = 'G';
}

void Glass::setNewWaterVolume(float waterVolume)
{
	this->waterVolume = waterVolume;
}

void Glass::setNewOilVolume(float oilVolume)
{
	this->oilVolume = oilVolume;
}

void Glass::setNewVolume(Fluid fluid)
{
	switch (fluid)
	{
	case water:
		if (waterVolume >= 1.0) waterVolume -= 1.0;
		else waterVolume = 0;
		break;
	case oil:
		if (oilVolume >= 0.5) oilVolume -= 0.5;
		else oilVolume = 0;
		break;
	}
}

char Glass::getGlassPosition()
{
	return glassPosition;
}

int Glass::getGlassCapacity()
{
	return glassCapacity;
}

// this method is just virtual, will overload in HotGlass
float Glass::getGlassTemperature(Player *)
{
	return 0.0f;
}

float Glass::getWaterVolume()
{
	return waterVolume <= 0 ? waterVolume = 0 : waterVolume;
}

float Glass::getOilVolume()
{
	return oilVolume <= 0 ? oilVolume = 0 : oilVolume;
}

float Glass::getGlassCharge()
{
	return waterVolume + oilVolume;
}

float Glass::getGlassFreeSpace()
{
	return glassCapacity - getGlassCharge();
}
