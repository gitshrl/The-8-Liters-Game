#pragma once
#include "Header.h"

class Button
{
public:
	Button();
	~Button();

	/* data members */
	ALLEGRO_BITMAP* button[5];
	ALLEGRO_BITMAP* hover;

	bool active;

	bool buttonHover[5];
	char buttonText[5];
	int x[5], y[5];

	int width, height;

	/* methods */
	void draw(int, int);
	void detectButtonHover(int, int);
	void reserveNumberOfButton(int);
	void destroy();

private:
	int numberOfButton;
};

