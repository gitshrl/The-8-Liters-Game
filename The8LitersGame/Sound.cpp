#include "Sound.h"



Sound::Sound()
{
	al_install_audio();
	al_init_acodec_addon();

	al_reserve_samples(1);
}


Sound::~Sound()
{
}

void Sound::init()
{
	main = al_load_sample("sound/main.ogg");
	mainInstance = al_create_sample_instance(main);
	al_set_sample_instance_playmode(mainInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(mainInstance, al_get_default_mixer());

	gamePlay = al_load_sample("sound/game-play.ogg");
	gamePlayInstance = al_create_sample_instance(gamePlay);
	al_set_sample_instance_playmode(gamePlayInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(gamePlayInstance, al_get_default_mixer());

	failed = al_load_sample("sound/game-over.ogg");
	failedInstance = al_create_sample_instance(failed);
	al_set_sample_instance_playmode(failedInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(failedInstance, al_get_default_mixer());

	solve = al_load_sample("sound/solve.ogg");
	solveInstance = al_create_sample_instance(solve);
	al_set_sample_instance_playmode(solveInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(solveInstance, al_get_default_mixer());

	click = al_load_sample("sound/click.ogg");
	clickInstance = al_create_sample_instance(click);
	al_set_sample_instance_playmode(clickInstance, ALLEGRO_PLAYMODE_ONCE);
	al_attach_sample_instance_to_mixer(clickInstance, al_get_default_mixer());

	water = al_load_sample("sound/water.ogg");
	waterInstance = al_create_sample_instance(water);
	al_set_sample_instance_playmode(waterInstance, ALLEGRO_PLAYMODE_ONCE);
	al_attach_sample_instance_to_mixer(waterInstance, al_get_default_mixer());
}

void Sound::play(ALLEGRO_SAMPLE_INSTANCE *instance)
{
	al_play_sample_instance(instance);
}

void Sound::stop(ALLEGRO_SAMPLE_INSTANCE *instance)
{
	al_stop_sample_instance(instance);
}

void Sound::destroy()
{
	al_destroy_sample(main);
	al_destroy_sample(gamePlay);
	al_destroy_sample(failed);
	al_destroy_sample(solve);
	al_destroy_sample(click);
	al_destroy_sample(water);

	al_destroy_sample_instance(mainInstance);
	al_destroy_sample_instance(gamePlayInstance);
	al_destroy_sample_instance(failedInstance);
	al_destroy_sample_instance(solveInstance);
	al_destroy_sample_instance(clickInstance);
	al_destroy_sample_instance(waterInstance);
}


