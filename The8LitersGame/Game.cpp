#include "Game.h"



Game::Game()
{
	Game::done = false;
	Game::key = false;
	Game::endtype = false;
	Game::type = true;
	Game::redraw = false;
	Game::isLoad = false;
	Game::top = false;
	Game::bottom = false;
	Game::ground = true;
	Game::bit = true;

	Game::selectedIndex = 1;
	Game::fps = 60;
}


Game::~Game()
{
	destroy();
}

void Game::start()
{
	switchDisplay();
}

int Game::getNumberOfSavedPlayer()
{
	playerFile.open("players-saved.txt", ios::in);

	int temp = 0;

	string playerName;
	float playerScore;
	int playerChance;

	while (true)
	{
		getline(playerFile, playerName);
		playerFile >> playerScore >> playerChance;

		if (playerFile.eof()) break;

		temp++;

		playerFile.ignore();
	}
	playerFile.close();

	return temp;
}

void Game::initAllegro()
{
	/* init allegro packages */
	if (!al_init()) {
		al_show_native_message_box(display, GAME_TITLE, NULL, "Failed to initialize Allegro5!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return;
	}

	/* init addons */
	al_init_image_addon();
	al_init_primitives_addon();
	al_init_font_addon();
	al_init_ttf_addon();

	/* init modules */
	al_install_audio();
	al_install_mouse();
	al_install_keyboard();

	/* init display */
	display = al_create_display(SCREEN_WIDTH, SCREEN_HEIGHT);
	al_set_window_position(display, 200, 100);
	al_set_window_title(display, "The8LitersGame");

	if (!display) {
		al_show_native_message_box(display, GAME_TITLE, NULL, "Failed to load display!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return;
	}

	/* init event queue */
	eventQueue = al_create_event_queue();
	al_register_event_source(eventQueue, al_get_display_event_source(display));
	al_register_event_source(eventQueue, al_get_mouse_event_source());
	al_register_event_source(eventQueue, al_get_keyboard_event_source());

	/* init timers */
	timer = al_create_timer(1 / fps);
	al_register_event_source(eventQueue, al_get_timer_event_source(timer));
	al_start_timer(timer);

	/* init colors */
	color.init();

	/* init sounds */
	backsound.init();

	/* set number of saved player */
	numberOfSavedPlayer = getNumberOfSavedPlayer();
}

void Game::initGlass()
{
	glasses.clear();

	Glass *newGlass;

	newGlass = new NormalGlass(NORMAL_GLASS_POSITION, NORMAL_GLASS_CAPACITY, NORMAL_GLASS_WATER_VOLUME, NORMAL_GLASS_OIL_VOLUME);
	glasses.push_back(newGlass);

	newGlass = new MagicGlass(MAGIC_GLASS_POSITION, MAGIC_GLASS_CAPACITY, MAGIC_GLASS_WATER_VOLUME, MAGIC_GLASS_OIL_VOLUME);
	glasses.push_back(newGlass);

	newGlass = new HotGlass(HOT_GLASS_POSITION, HOT_GLASS_CAPACITY, HOT_GLASS_WATER_VOLUME, HOT_GLASS_OIL_VOLUME, HOT_GLASS_INITIAL_TEMPERATURE);
	glasses.push_back(newGlass);
}

void Game::initPlayer(string playerName)
{
	isLoad = false; // define to not load mode.

	Player *newPlayer;

	newPlayer = new Player(playerName, INITIAL_PLAYER_SCORE, INITIAL_PLAYER_CHANCE);
	players.push_back(newPlayer);

	playerSelected = newPlayer; // set new player as selected player
}

void Game::loadBitmap()
{
	/* load background */
	mainMenu.background = al_load_bitmap(MAINMENU_BACKGROUND_IMAGE);
	mainMenu.font = al_load_font(DEFAULT_FONT_TYPE, DEFAULT_FONT_SIZE, NULL);

	newGame.background = al_load_bitmap(NEWGAME_BACKGROUND_IMAGE);
	newGame.font = al_load_font(INSERT_TEXT_FONT_TYPE, INSERT_TEXT_FONT_SIZE, NULL);

	loadGame.background = al_load_bitmap(LOADGAME_BACKGROUND_IMAGE);
	loadGame.font = al_load_font(DEFAULT_FONT_TYPE, DEFAULT_FONT_SIZE, NULL);

	leaderboard.background = al_load_bitmap(LEADERBOARD_BACKGROUND_IMAGE);
	leaderboard.font = al_load_font(DEFAULT_FONT_TYPE, DEFAULT_FONT_SIZE, NULL);
	leader.font = al_load_font(TOP_LEADER_FONT_TYPE, TOP_LEADER_FONT_SIZE, NULL);

	gamePlay.background = al_load_bitmap(GAMEPLAY_BACKGROUND_IMAGE);
	gamePlay.font = al_load_font(GAME_PLAY_FONT_TYPE, GAME_PLAY_FONT_SIZE, NULL);

	name.font = al_load_font(NAME_FONT_TYPE, NAME_FONT_SIZE, NULL);
	chance.font = al_load_font(NAME_FONT_TYPE, CHANCE_FONT_SIZE, NULL);
	guide.font = al_load_font(NAME_FONT_TYPE, GUIDE_FONT_SIZE, NULL);

	guide.background = al_load_bitmap(GUIDE_BACKGROUND_IMAGE);
	finish.background = al_load_bitmap(FINISH_BACKGROUND_IMAGE);

	status.font = al_load_font(GAME_STATUS_FONT_TYPE, GAME_STATUS_FONT_SIZE, NULL);

	/* load buttons */
	for (int i = 0; i < NUMBER_BUTTON_MENU; i++) {
		buttonTextColor[i] = color.white;
		menu.button[i] = al_load_bitmap(BUTTON_SHAPE_IMAGE);
		menu.width = al_get_bitmap_width(menu.button[i]);
		menu.height = al_get_bitmap_height(menu.button[i]);
	}
	menu.hover = al_load_bitmap(BUTTON_SHAPE_HOVER_IMAGE);
	menu.reserveNumberOfButton(NUMBER_BUTTON_MENU);

	for (int i = 0; i < NUMBER_BUTTON_PLAY; i++) {
		play.button[i] = al_load_bitmap(PLAY_BUTTON_IMAGE);
		play.width = al_get_bitmap_width(play.button[i]);
		play.height = al_get_bitmap_height(play.button[i]);
	}
	play.hover = al_load_bitmap(PLAY_BUTTON_HOVER_IMAGE);
	play.reserveNumberOfButton(NUMBER_BUTTON_PLAY);

	for (int i = 0; i < NUMBER_BUTTON_BACK; i++) {
		back.button[i] = al_load_bitmap(BACK_BUTTON_IMAGE);
		back.width = al_get_bitmap_width(back.button[i]);
		back.height = al_get_bitmap_height(back.button[i]);
	}
	back.hover = al_load_bitmap(BACK_BUTTON_HOVER_IMAGE);
	back.reserveNumberOfButton(NUMBER_BUTTON_BACK);

	int n; // set number of loops
	getNumberOfSavedPlayer() > 5 ? n = 5 : n = getNumberOfSavedPlayer();

	for (int i = 0; i < n; i++) {
		load.button[i] = al_load_bitmap(LOAD_BUTTON_IMAGE);
		load.width = al_get_bitmap_width(load.button[i]);
		load.height = al_get_bitmap_height(load.button[i]);
	}
	load.hover = al_load_bitmap(LOAD_BUTTON_HOVER_IMAGE);
	load.reserveNumberOfButton(n);

	/* load buttons on game play */
	Button button[6] = { repeat, save, home, tap, restart, help };

	string path1[6] = { REPEAT_BUTTON_IMAGE, SAVE_BUTTON_IMAGE, HOME_BUTTON_IMAGE, TAP_BUTTON_IMAGE, RESTART_BUTTON_IMAGE, HELP_BUTTON_IMAGE };
	string path2[6] = { REPEAT_BUTTON_HOVER_IMAGE, SAVE_BUTTON_HOVER_IMAGE, HOME_BUTTON_HOVER_IMAGE, TAP_BUTTON_HOVER_IMAGE, RESTART_BUTTON_HOVER_IMAGE, HELP_BUTTON_HOVER_IMAGE };

	for (int i = 0; i < 6; i++) {
		button[i].button[0] = al_load_bitmap(path1[i].c_str());
		button[i].width = al_get_bitmap_width(button[i].button[0]);
		button[i].height = al_get_bitmap_height(button[i].button[0]);
		button[i].hover = al_load_bitmap(path2[i].c_str());
		button[i].reserveNumberOfButton(1);
	}
	repeat = button[0]; save = button[1]; home = button[2]; tap = button[3]; restart = button[4]; help = button[5];

	/* load initial glass bitmap */
	string path3[3] = { GLASS_1_6_IMAGE, GLASS_2_5_IMAGE, GLASS_3_5_IMAGE };
	string path4[3] = { GLASS_1_HOVER_IMAGE, GLASS_2_HOVER_IMAGE, GLASS_3_HOVER_IMAGE };

	for (int i = 0; i < 3; i++) {
		glassTop[i].button[0] = al_load_bitmap(path3[i].c_str());
		glassTop[i].width = al_get_bitmap_width(glassTop[i].button[0]);
		glassTop[i].height = al_get_bitmap_height(glassTop[i].button[0]);
		glassTop[i].hover = al_load_bitmap(path4[i].c_str());
		glassTop[i].reserveNumberOfButton(1);

		glassBot[i].button[0] = al_load_bitmap(path3[i].c_str());
		glassBot[i].width = al_get_bitmap_width(glassBot[i].button[0]);
		glassBot[i].height = al_get_bitmap_height(glassBot[i].button[0]);
		glassBot[i].hover = al_load_bitmap(path4[i].c_str());
		glassBot[i].reserveNumberOfButton(1);
	}
}

void Game::loadGlassInfo()
{
	glassList.clear();

	Glass *newGlass;

	glassFile.open("glasses-saved.txt", ios::in);

	/* temporary variables */
	string glassType;
	char glassPosition;
	int glassCapacity;
	float waterVolume;
	float oilVolume;
	float glassTemperature;

	while (true) {
		getline(glassFile, glassType); // precondition to decide what glass is it.

		if (glassFile.eof()) break;

		if (glassType == NORMAL_GLASS_TYPE) {
			glassFile >> glassPosition >> glassCapacity >> waterVolume >> oilVolume;
			newGlass = new NormalGlass(glassPosition, glassCapacity, waterVolume, oilVolume);
			glassList.push_back(newGlass);
			glassFile.ignore();
		}
		else if (glassType == MAGIC_GLASS_TYPE) {
			glassFile >> glassPosition >> glassCapacity >> waterVolume >> oilVolume;
			newGlass = new MagicGlass(glassPosition, glassCapacity, waterVolume, oilVolume);
			glassList.push_back(newGlass);
			glassFile.ignore();
		}
		else if (glassType == HOT_GLASS_TYPE) {
			glassFile >> glassPosition >> glassCapacity >> waterVolume >> oilVolume >> glassTemperature;
			newGlass = new HotGlass(glassPosition, glassCapacity, waterVolume, oilVolume, glassTemperature);
			glassList.push_back(newGlass);
			glassFile.ignore();
		}
	}

	glassFile.close();
}

void Game::loadPlayerInfo()
{
	playerList.clear();
	players.clear();

	Player *newPlayer;

	playerFile.open("players-saved.txt", ios::in);

	/* temporary variables */
	string playerName;
	float playerScore;
	int playerChance;

	while (true)
	{
		getline(playerFile, playerName);
		playerFile >> playerScore >> playerChance;

		if (playerFile.eof()) break;

		newPlayer = new Player(playerName, playerScore, playerChance);
		playerList.push_back(newPlayer);
		players.push_back(newPlayer);

		playerFile.ignore();
	}
	playerFile.close();

	// to set latest players to show on display
	if (playerList.size() > 5) {
		playerList.erase(playerList.begin() + 0, playerList.begin() + (playerList.size() - 5));
	}
}

void Game::loadGameInfo()
{
	isLoad = true; // define to load mode

	loadPlayerInfo();
	loadGlassInfo();
}

void Game::showLeaderboard()
{
	playerRank.clear();

	Player *loadPlayer;

	playerFile.open("players-saved.txt", ios::in);

	/* temporary variable */
	string playerName;
	float playerScore;
	int playerChance;

	while (true) {
		getline(playerFile, playerName);
		playerFile >> playerScore;
		playerFile >> playerChance;

		if (playerFile.eof()) break;

		loadPlayer = new Player(playerName, playerScore, playerChance);
		playerRank.push_back(loadPlayer);

		playerFile.ignore();
	}
	playerFile.close();


	/* draw text on display */
	int a, b, position = 110;

	if (playerRank.size() > 5) { a = playerRank.size(); b = a - 5; }
	else { a = playerRank.size(); b = 0; }

	for (int i = a - 1; i >= b; i--) {
		string str = playerRank[i]->getPlayerName();

		if (playerRank[i]->getPlayerScore() == 65) {
			al_draw_text(leader.font, color.white, SCREEN_WIDTH / 2.0, position += 30, ALLEGRO_ALIGN_CENTER, str.c_str());
		}
	}
}

void Game::resetGameHistory()
{
	// to remove all file .txt contents
	playerFile.open("players-saved.txt", ios::out | ios::trunc);
	glassFile.open("glasses-saved.txt", ios::out | ios::trunc);

	playerFile.close();
	glassFile.close();
}

void Game::saveGameInfo()
{
	playerSelected->savePlayerInfo();

	for (int i = 0; i < 3; i++) {
		glasses[i]->saveGlassInfo();
	}
}

void Game::mixingProcess()
{
	playerSelected->setNewChance(running); // reducing player chances during game is running.

	/* temporary variables */
	float topGlassOilVolume = glassOnTop->getOilVolume();
	float topGlasswaterVolume = glassOnTop->getWaterVolume();
	float botGlassOilVolume = glassOnBot->getOilVolume();
	float botGlasswaterVolume = glassOnBot->getWaterVolume();

	if (glassOnBot->getGlassFreeSpace() < glassOnTop->getGlassCharge()) {
		if (glassOnBot->getGlassFreeSpace()> glassOnTop->getOilVolume()) {
			glassOnBot->setNewOilVolume(botGlassOilVolume + topGlassOilVolume);
			glassOnTop->setNewOilVolume(0);
			if (glassOnBot->getGlassFreeSpace() != 0) {
				if (glassOnBot->getGlassFreeSpace() > glassOnTop->getWaterVolume()) {
					glassOnBot->setNewWaterVolume(botGlasswaterVolume + topGlasswaterVolume);
					glassOnTop->setNewWaterVolume(0);
				}
				else {
					float temp = glassOnBot->getGlassFreeSpace();
					glassOnBot->setNewWaterVolume(botGlasswaterVolume + temp);
					glassOnTop->setNewWaterVolume(topGlasswaterVolume - temp);
				}
			}
		}
		else {
			float temp = glassOnBot->getGlassFreeSpace();
			glassOnBot->setNewOilVolume(botGlassOilVolume + temp);
			glassOnTop->setNewOilVolume(topGlassOilVolume -= temp);
		}
	}
	else {
		glassOnTop->setNewOilVolume(0);
		glassOnTop->setNewWaterVolume(0);
		glassOnBot->setNewOilVolume(botGlassOilVolume + topGlassOilVolume);
		glassOnBot->setNewWaterVolume(botGlasswaterVolume + topGlasswaterVolume);
	}
}

void Game::temperatureEffect()
{	
	if (glasses[botIndex + 2]->getGlassCharge() != 0) {
		if (glasses[botIndex + 2]->getGlassTemperature(playerSelected) >= WATER_BOILING_POINT) {

			glasses[botIndex + 2]->setNewVolume(water);
		}
		if (glasses[botIndex + 2]->getGlassTemperature(playerSelected) >= OIL_BOILING_POINT) {
			glasses[botIndex + 2]->setNewVolume(oil);
		}
	}
	else glasses[botIndex + 2]->getGlassTemperature(playerSelected);
}

void Game::getOutGlasses()
{
	for (int i = 0; i < glasses.size(); i++) {
		glasses[i]->getOutGlasses();
	}
}

void Game::switchGlassBitmap(int i)
{
	string path1, path2, path3;

	if (glasses[i]->getGlassCharge() == 0) {
		path1 = GLASS_1_5_IMAGE;
		path2 = GLASS_2_5_IMAGE;
		path3 = GLASS_3_5_IMAGE;
	}
	else if (glasses[i]->getOilVolume() != 0 && glasses[i]->getWaterVolume() != 0) {
		path1 = GLASS_1_6_IMAGE;
		path2 = GLASS_2_6_IMAGE;
		path3 = GLASS_3_6_IMAGE;
	}
	else if (glasses[i]->getOilVolume() == glasses[i]->getGlassCapacity() || glasses[i]->getWaterVolume() == glasses[i]->getGlassCapacity()) {
		if (glasses[i]->getOilVolume() == glasses[i]->getGlassCapacity()) {
			path1 = GLASS_1_2_IMAGE;
			path2 = GLASS_2_2_IMAGE;
			path3 = GLASS_3_2_IMAGE;
		}
		else {
			path1 = GLASS_1_1_IMAGE;
			path2 = GLASS_2_1_IMAGE;
			path3 = GLASS_3_1_IMAGE;
		}
	}
	else if (glasses[i]->getOilVolume() == 0 || glasses[i]->getWaterVolume() == 0) {
		if (glasses[i]->getOilVolume() == 0) {
			path1 = GLASS_1_3_IMAGE;
			path2 = GLASS_2_3_IMAGE;
			path3 = GLASS_3_3_IMAGE;
		}
		else {
			path1 = GLASS_1_4_IMAGE;
			path2 = GLASS_2_4_IMAGE;
			path3 = GLASS_3_4_IMAGE;
		}
	}

	string path4[3] = { path1, path2, path3 };
	string path5[3] = { GLASS_1_HOVER_IMAGE, GLASS_2_HOVER_IMAGE, GLASS_3_HOVER_IMAGE };


	int idx; // a temporary variable to handle index.

	if (i == botIndex) idx = 0;
	else if (i == botIndex + 1) idx = 1;
	else if (i == botIndex + 2) idx = 2;

	glassTop[idx].button[0] = al_load_bitmap(path4[idx].c_str());
	glassTop[idx].width = al_get_bitmap_width(glassTop[idx].button[0]);
	glassTop[idx].height = al_get_bitmap_height(glassTop[idx].button[0]);
	glassTop[idx].hover = al_load_bitmap(path5[idx].c_str());

	glassBot[idx].button[0] = al_load_bitmap(path4[idx].c_str());
	glassBot[idx].width = al_get_bitmap_width(glassBot[idx].button[0]);
	glassBot[idx].height = al_get_bitmap_height(glassBot[idx].button[0]);
	glassBot[idx].hover = al_load_bitmap(path5[idx].c_str());
}

void Game::insertPlayerName()
{
	ALLEGRO_FONT* font = al_create_builtin_font();
	ALLEGRO_EVENT_QUEUE* q = al_create_event_queue();

	al_register_event_source(q, al_get_keyboard_event_source());
	al_register_event_source(q, al_get_display_event_source(display));

	ALLEGRO_USTR* str = al_ustr_new("");

	int pos = (int)al_ustr_size(str);

	while (type) {
		newGame.draw();
		play.draw(SCREEN_WIDTH / 2.0 - (menu.width / 10.0), 160);

		al_draw_ustr(newGame.font, color.black, SCREEN_WIDTH / 1.95, 195, ALLEGRO_ALIGN_CENTRE, str);
		al_flip_display();

		ALLEGRO_EVENT e;
		al_wait_for_event(q, &e);

		// condition to endtype.
		if ((pos > 0 && e.keyboard.keycode == ALLEGRO_KEY_ENTER) || pos > 8) {
			type = false;
			endtype = true;
		}

		if (e.type == ALLEGRO_EVENT_KEY_CHAR) {
			if (e.keyboard.unichar >= 32) {
				pos += al_ustr_append_chr(str, e.keyboard.unichar);
			}
			else if (e.keyboard.keycode == ALLEGRO_KEY_BACKSPACE) {
				if (al_ustr_prev(str, &pos)) al_ustr_truncate(str, pos);
			}
		}
	}

	pos = 0; // set position to 0;

	initPlayer(al_cstr(str)); // define input as new player.
	playerName = al_cstr(str); // set string input as player name.
}

void Game::playerScore(int topSelectedIndex, int botSelectedIndex)
{
	// count player scores by each steps that right.

	switch (playerSelected->getPlayerChance())
	{
	case 1: case 5: case 7: case 11: case 13:
		if (topSelectedIndex == 1 && botSelectedIndex == 3)
			playerSelected->setNewScore(doingRightStep);
		else
			playerSelected->setNewScore(doingWrongStep);
		break;
	case 3: case 9:
		if (topSelectedIndex == 2 && botSelectedIndex == 1)
			playerSelected->setNewScore(doingRightStep);
		else
			playerSelected->setNewScore(doingWrongStep);
		break;
	case 2: case 4: case 6: case 8: case 10: case 12:
		if (topSelectedIndex == 3 && botSelectedIndex == 2)
			playerSelected->setNewScore(doingRightStep);
		else
			playerSelected->setNewScore(doingWrongStep);
		break;
	}
}

void Game::gameStatus()
{
	playerSelected->getPlayerChance() == 0 ? gameOver = true : gameOver = false;

	int temp = 0;
	for (int i = botIndex; i < topIndex; i++) {
		temp += glasses[i]->getGlassCharge();
	}

	if (gameOver) {
		finish.draw();
		backsound.stop(backsound.gamePlayInstance);

		if (glasses[selectedIndex - 1]->getGlassCharge() == DEFAULT_CHARGE_TO_WIN && temp == 16) {
			// play backsound when successfully to solve.
			backsound.play(backsound.solveInstance);

			al_draw_text(status.font, color.white, SCREEN_WIDTH / 1.95, 150, ALLEGRO_ALIGN_CENTER, "Great!!!");
			
			home.draw(300, 125);
			home.detectButtonHover(curX, curY);
		}
		else {
			// play backsound when failed to solve.
			backsound.play(backsound.failedInstance);

			al_draw_text(status.font, color.white, SCREEN_WIDTH / 1.95, 150, ALLEGRO_ALIGN_CENTER, "You failed!!!");
			
			home.draw(280, 125);
			home.detectButtonHover(curX, curY);
			
			restart.draw(340, 125);
			restart.detectButtonHover(curX, curY);
		}
	}
}

void Game::manualGame(Help help)
{
	switch (help)
	{
	case rule:
		guide.draw();
		al_draw_text(guide.font, color.white, 280, 75, ALLEGRO_ALIGN_LEFT, "~GAME RULE~");
		al_draw_text(guide.font, color.white, 61, 90, ALLEGRO_ALIGN_LEFT, "** There are 3 cups of 16L, 11L, 6L. The cup of 16L was filled with oil and water.");
		al_draw_text(guide.font, color.white, 82, 105, ALLEGRO_ALIGN_LEFT, "Pour out 8L of liquids in one of 3 cups.");
		al_draw_text(guide.font, color.white, 61, 125, ALLEGRO_ALIGN_LEFT, "** Oil has high priority to pour than water.");
		al_draw_text(guide.font, color.white, 61, 145, ALLEGRO_ALIGN_LEFT, "** In third cup, there are temperature effect that will evaporates liquids.");
		al_draw_text(guide.font, color.white, 61, 165, ALLEGRO_ALIGN_LEFT, "** Press 'ESC' to back home, and your game will save automaticlly.");
		al_draw_text(guide.font, color.white, 210, 200, ALLEGRO_ALIGN_LEFT, "-enjoy your game, wish you luck!-");
		break;
	case instruction:
		if (top) {
			al_draw_text(guide.font, color.black, 70, 327, ALLEGRO_ALIGN_LEFT, "press 'DOWN' to repeat steps. -->");
			if (bottom) {
				if (ground) al_draw_text(guide.font, color.black, 370, 129, ALLEGRO_ALIGN_LEFT, "<-- press 'TAP' to pour fluids.");
			}
			else {
				al_draw_text(guide.font, color.black, 70, 282, ALLEGRO_ALIGN_LEFT, "Click another glass");
				al_draw_text(guide.font, color.black, 70, 302, ALLEGRO_ALIGN_LEFT, "to put on bottom, or ");
			}
		}
		else {
			al_draw_text(guide.font, color.black, 20, 400, ALLEGRO_ALIGN_LEFT, "Click one of these");
			al_draw_text(guide.font, color.black, 20, 420, ALLEGRO_ALIGN_LEFT, " glasses to put on top. --> ");
		}
		break;
	}
}

void Game::switchDisplay()
{
	initAllegro();

	loadBitmap();

	initGlass();

	while (!done) {
		al_wait_for_event(eventQueue, &event);

		/* close diplay button */
		if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			result = al_show_native_message_box(display, GAME_TITLE, "Warning!", "Are you sure to leave the game?", NULL, ALLEGRO_MESSAGEBOX_YES_NO);
			if (result == 1) done = true; // display close when result equal to 1
		}

		/* to get cursor position */
		if (event.type == ALLEGRO_EVENT_MOUSE_AXES) {
			curX = event.mouse.x;
			curY = event.mouse.y;
		}

		if (event.type == ALLEGRO_EVENT_KEY_UP) {
			if (event.keyboard.keycode == ALLEGRO_KEY_ESCAPE && key) {
				backsound.stop(backsound.gamePlayInstance);
				backsound.stop(backsound.solveInstance);
				backsound.stop(backsound.failedInstance);

				glassSelectedTop.height = -1;
				glassSelectedBot.height = -1;

				getOutGlasses();
				saveGameInfo();

				for (int i = 0; i < 3; i++) {
					glassTop[i].buttonHover[0] = false;
					glassBot[i].buttonHover[0] = false;
				}

				// define next display to show
				mainMenu.done = false;
				endtype = false;
				type = true;
				top = false;
				bottom = false;
				ground = true;
				bit = true;
				key = false;
			}
		}

		if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
			/* main menu display */
			if (!mainMenu.done) {
				if (event.mouse.button & 1) {
					/* set next display to show */
					if (menu.buttonHover[0]) { newGame.done = false; mainMenu.done = true; loadGame.done = true; leaderboard.done = true; reset.active = false; endtype = false; type = true; }
					else if (menu.buttonHover[1]) { loadGame.done = false; mainMenu.done = true; newGame.done = true; leaderboard.done = true; reset.active = false; }
					else if (menu.buttonHover[2]) { leaderboard.done = false; mainMenu.done = true; newGame.done = true; loadGame.done = true; reset.active = false; }
					else if (menu.buttonHover[3]) { reset.active = true; mainMenu.done = true; newGame.done = true; loadGame.done = true; leaderboard.done = true; }
					else if (menu.buttonHover[4]) done = true;

					for (int i = 0; i < 3; i++) {
						glassTop[i].active = false;
						glassBot[i].active = false;
					}

					// define next diplasy when back to home.
					tap.active = false;
					repeat.active = false;
					save.active = false;
					home.active = false;
					selectedIndex = 1;
					isLoad = false;
				}
				goto flag;
			}

			/* load game display */
			if (!loadGame.done) {
				for (int i = 0; i < getNumberOfSavedPlayer(); i++) {
					if (event.mouse.button & 1) {
						if (load.buttonHover[i]) {
							if (getNumberOfSavedPlayer() > 5)selectedIndex = i + 1 + (getNumberOfSavedPlayer() - 5);
							else selectedIndex = i + 1;

							playerSelected = players[selectedIndex - 1];
							playerName = playerSelected->getPlayerName();

							// prevent to open game play when players game already finished.
							if (playerSelected->getPlayerChance() == 0) { gamePlay.done = true; mainMenu.done = false; }

							// set next display to show.
							gamePlay.done = false;
							loadGame.done = true;
							break;
						}
					}
				}
				if (event.mouse.button & 1) { if (back.buttonHover[0]) { mainMenu.done = false; newGame.done = true; leaderboard.done = true; } }
				goto flag;
			}

			/* new game display */
			if (!newGame.done) {
				if (event.mouse.button & 1) {
					if (play.buttonHover[0]) { loadBitmap(); initGlass(); gamePlay.done = false; mainMenu.done = true; newGame.done = true; leaderboard.done = true;  reset.active = false; }
				}
				goto flag;
			}

			/* leaderboard display : has been changed to latest winners */
			if (!leaderboard.done) {
				if (event.mouse.button & 1) {
					if (back.buttonHover[0]) { mainMenu.done = false; newGame.done = true; leaderboard.done = true; }
				}
				goto flag;
			}

			/* game play */
			if (!gamePlay.done) {
				int temp = botIndex;

				if (!top) {
					if (event.mouse.button & 1) {
						if (glassTop[0].buttonHover[0]) {
							backsound.stop(backsound.clickInstance);
							backsound.play(backsound.clickInstance);

							glassTop[0].active = true; glassTop[1].active = false; glassTop[2].active = false;
							glassSelectedTop.height = glassTop[0].height;
							topIndex = temp;
							glassOnTop = glasses[topIndex];
							topSelectedIndex = 1;
							top = true;
						}
						else if (glassTop[1].buttonHover[0]) {
							backsound.stop(backsound.clickInstance);
							backsound.play(backsound.clickInstance);

							glassTop[0].active = false; glassTop[1].active = true; glassTop[2].active = false;
							glassSelectedTop.height = glassTop[1].height;
							topIndex = temp + 1;
							glassOnTop = glasses[topIndex];
							topSelectedIndex = 2;
							top = true;
						}
						else if (glassTop[2].buttonHover[0]) {
							backsound.stop(backsound.clickInstance);
							backsound.play(backsound.clickInstance);

							glassTop[0].active = false; glassTop[1].active = false; glassTop[2].active = true;
							glassSelectedTop.height = glassTop[2].height;
							topIndex = temp + 2;
							glassOnTop = glasses[topIndex];
							topSelectedIndex = 3;
							top = true;
						}
					}
				}

				else if (!bottom) {
					if (event.mouse.button & 1) {
						if (glassBot[0].buttonHover[0]) {
							backsound.stop(backsound.clickInstance);
							backsound.play(backsound.clickInstance);

							glassBot[0].active = true; glassBot[1].active = false; glassBot[2].active = false;
							glassSelectedBot.height = glassBot[0].height;
							botIndex = temp;
							glassOnBot = glasses[botIndex];
							botSelectedIndex = 1;
							bottom = true;
						}
						else if (glassBot[1].buttonHover[0]) {
							backsound.stop(backsound.clickInstance);
							backsound.play(backsound.clickInstance);

							glassBot[0].active = false; glassBot[1].active = true; glassBot[2].active = false;
							glassSelectedBot.height = glassBot[1].height;
							botIndex = temp + 1;
							glassOnBot = glasses[botIndex];
							botSelectedIndex = 2;
							bottom = true;
						}
						else if (glassBot[2].buttonHover[0]) {
							backsound.stop(backsound.clickInstance);
							backsound.play(backsound.clickInstance);

							glassBot[0].active = false; glassBot[1].active = false; glassBot[2].active = true;
							glassSelectedBot.height = glassBot[2].height;
							botIndex = temp + 2;
							glassOnBot = glasses[botIndex];
							botSelectedIndex = 3;
							bottom = true;
						}
					}
				}

				if (event.mouse.button & 1) {
					if (repeat.buttonHover[0]) {
						backsound.stop(backsound.clickInstance);
						backsound.play(backsound.clickInstance);

						top = false; bottom = false;

						glassSelectedTop.height = -1;
						glassSelectedBot.height = -1;

						for (int i = 0; i < 3; i++) {
							glassTop[i].buttonHover[0] = false;
							glassBot[i].buttonHover[0] = false;
						}

						getOutGlasses(); // put all glasses to ground.
						ground = true;
						repeat.active = true; // set repeat button to active mode.
					}
				}

				if (event.mouse.button & 1) {
					if (tap.buttonHover[0]) tap.active = true;
				}

				if (event.mouse.button & 1) {
					if (home.buttonHover[0]) {
						// stop backsound when back to home
						backsound.stop(backsound.gamePlayInstance);
						backsound.stop(backsound.solveInstance);
						backsound.stop(backsound.failedInstance);

						loadBitmap(); // set bitmap to default
						initGlass(); // set glasses to default

						playerSelected->setNewChance(restarted);
						playerSelected->setNewScore(restartTheGame);

						glassSelectedTop.height = -1;
						glassSelectedBot.height = -1;

						for (int i = 0; i < 3; i++) {
							glassTop[i].buttonHover[0] = false;
							glassBot[i].buttonHover[0] = false;
						}

						getOutGlasses();

						/* set next display tho show */
						mainMenu.done = false;
						endtype = false; // allow to acces insert player name methods
						type = true; // allow to type again
						top = false; bottom = false;
						ground = true;
						home.buttonHover[0] = false;
					}
				}

				if (event.mouse.button & 1) {
					if (restart.buttonHover[0]) {
						// stop backsound when replay game
						backsound.stop(backsound.solveInstance);
						backsound.stop(backsound.failedInstance);

						loadBitmap();
						initGlass();

						playerSelected->setNewChance(restarted);
						playerSelected->setNewScore(restartTheGame);

						glassSelectedBot.height = -1;
						glassSelectedTop.height = -1;

						/* set next display tho show */
						endtype = false; // allow to acces insert player name methods.
						type = true; // allow to type again.
						top = false;
						bottom = false;
						restart.buttonHover[0] = false;
						ground = true;
					}
				}

				if (event.mouse.button & 1) {
					help.active = false;
					if (help.buttonHover[0]) {
						backsound.stop(backsound.clickInstance);
						backsound.play(backsound.clickInstance);

						help.active = true;
					}
				}

				goto flag;
			}

		}

		flag:

		if (event.type == ALLEGRO_EVENT_TIMER) {
			if (event.timer.source == timer) redraw = true;
		}

		if (redraw) {
			if (!mainMenu.done) {
				backsound.play(backsound.mainInstance);

				for (int i = 0; i < NUMBER_BUTTON_MENU; i++) {
					if (menu.buttonHover[i]) buttonTextColor[i] = color.black;
					else buttonTextColor[i] = color.orange;
				}

				mainMenu.draw();
				menu.draw(SCREEN_WIDTH / 2.0 - (menu.width / 2.0), 50);
				menu.detectButtonHover(curX, curY);

				for (int i = 0; i < NUMBER_BUTTON_MENU; i++) {
					string str[5] = { "NEW GAME" , "LOAD GAME", "LATEST WINNERS", "RESET HISTORY", "QUIT" };
					al_draw_text(mainMenu.font, buttonTextColor[i], SCREEN_WIDTH / 2.0, menu.y[i] + DEFAULT_FONT_SIZE - 17.0, ALLEGRO_ALIGN_CENTER, str[i].c_str());
				}
			}
			else if (!newGame.done) {
				newGame.draw();

				play.draw(SCREEN_WIDTH / 2.0 - (menu.width / 10.0), 160);
				play.detectButtonHover(curX, curY);

				al_draw_text(newGame.font, color.black, SCREEN_WIDTH / 1.95, 195, ALLEGRO_ALIGN_CENTER, playerName.c_str());

				if (!endtype) {
					insertPlayerName();
				}
			}
			else if (!loadGame.done) {
				loadGameInfo();

				int a = 0, b = playerList.size();

				if (playerList.size() > 5) { a = playerList.size() - 5; b = a + 5; }

				if (bit) { loadBitmap(); bit = false; }

				for (int i = a; i < b; i++) {
					if (load.buttonHover[i]) buttonTextColor[i] = color.black;
					else buttonTextColor[i] = color.white;
				}

				for (int i = 0; i < getNumberOfSavedPlayer(); i++) load.buttonHover[i] = false;

				loadGame.draw();

				load.draw(SCREEN_WIDTH / 2.0 - (menu.width / 2.0) - 50, 50);
				load.detectButtonHover(curX, curY);

				back.draw(SCREEN_WIDTH / 2.0 - (menu.width / 16.0) - 10, 240);
				back.detectButtonHover(curX, curY);

				for (int i = a; i < b; i++) {
					string str = (playerList[i]->getPlayerName() + " (" + to_string(playerList[i]->getPlayerChance()) + ")").c_str();
					al_draw_text(loadGame.font, buttonTextColor[i], SCREEN_WIDTH / 2.0, menu.y[i] + DEFAULT_FONT_SIZE - 17.0, ALLEGRO_ALIGN_CENTRE, str.c_str());
				}
			}
			else if (!leaderboard.done) {
				leaderboard.draw();

				back.draw(SCREEN_WIDTH / 2.0 - (menu.width / 16.0), 230);
				back.detectButtonHover(curX, curY);

				showLeaderboard();
			}
			else if (reset.active) {
				result = al_show_native_message_box(display, GAME_TITLE, "Warning!", "Are you sure to delete all players saved?", NULL, ALLEGRO_MESSAGEBOX_YES_NO);
				if (result == 1) resetGameHistory();

				reset.active = false;
				mainMenu.done = false;
			}
			else if (!gamePlay.done) {
				backsound.stop(backsound.mainInstance);
				backsound.play(backsound.gamePlayInstance);

				gamePlay.draw();

				repeat.draw(317, 246);
				repeat.detectButtonHover(curX, curY);

				tap.draw(346, 44);
				if (ground && bottom) tap.detectButtonHover(curX, curY);

				help.draw(515, 300);
				help.detectButtonHover(curX, curY);

				if (save.active) { saveGameInfo(); save.active = false; }

				if (top) {
					if (glassTop[0].active) glassOnTop->setNewPosition(putOnTop, glassTop[0]);
					else if (glassTop[1].active) glassOnTop->setNewPosition(putOnTop, glassTop[1]);
					else if (glassTop[2].active) glassOnTop->setNewPosition(putOnTop, glassTop[2]);
				}

				if (bottom) {
					if (glassBot[0].active) { if (glassOnTop != glassOnBot) glassOnBot->setNewPosition(putOnBottom, glassBot[0]); }
					else if (glassBot[1].active) { if (glassOnTop != glassOnBot) glassOnBot->setNewPosition(putOnBottom, glassBot[1]); }
					else if (glassBot[2].active) { if (glassOnTop != glassOnBot) glassOnBot->setNewPosition(putOnBottom, glassBot[2]); }

					if (tap.active) {
						backsound.stop(backsound.clickInstance);
						if (glassOnTop->getGlassCharge() != 0) {
							backsound.stop(backsound.waterInstance);
							backsound.play(backsound.waterInstance);
						}
						else backsound.play(backsound.clickInstance);

						playerScore(topSelectedIndex, botSelectedIndex);
						mixingProcess();
						temperatureEffect();

						for (int i = botIndex; i < topIndex; i++) switchGlassBitmap(i);

						playerSelected->getPlayerScore() == 65? save.active = true : save.active = false;

						tap.active = false;
						tap.buttonHover[0] = false;
						ground = false;
					}
				}

				if (!top) {
					if (glassSelectedBot.height != glassBot[0].height && glassSelectedTop.height != glassTop[0].height) {
						glassTop[0].draw(241, 300);
						glassTop[0].detectButtonHover(curX, curY);
					}
					if (glassSelectedBot.height != glassBot[1].height && glassSelectedTop.height != glassTop[1].height) {
						glassTop[1].draw(308, 310);
						glassTop[1].detectButtonHover(curX, curY);
					}
					if (glassSelectedBot.height != glassBot[2].height && glassSelectedTop.height != glassTop[2].height) {
						glassTop[2].draw(373, 320);
						glassTop[2].detectButtonHover(curX, curY);
					}
				}
				else {
					if (glassSelectedBot.height != glassBot[0].height && glassSelectedTop.height != glassTop[0].height) {
						glassBot[0].draw(241, 300);
						glassBot[0].detectButtonHover(curX, curY);
					}
					if (glassSelectedBot.height != glassBot[1].height && glassSelectedTop.height != glassTop[1].height) {
						glassBot[1].draw(308, 310);
						glassBot[1].detectButtonHover(curX, curY);
					}
					if (glassSelectedBot.height != glassBot[2].height && glassSelectedTop.height != glassTop[2].height) {
						glassBot[2].draw(373, 320);
						glassBot[2].detectButtonHover(curX, curY);
					}
				}


				string str = "Player: " + playerName;
				al_draw_text(name.font, color.white, 5, 5, ALLEGRO_ALIGN_LEFT, str.c_str());

				string scr = "Score: " + text.decimalPrecisionPoint(4, playerSelected->getPlayerScore());
				al_draw_text(name.font, color.white, 5, 37, ALLEGRO_ALIGN_LEFT, scr.c_str());

				string chc = to_string(playerSelected->getPlayerChance());
				al_draw_text(chance.font, color.white, 320, 13, ALLEGRO_ALIGN_LEFT, chc.c_str());

				botIndex = (selectedIndex - 1) * 3;
				topIndex = botIndex + 3;

				if (isLoad) {
					glasses = glassList; getOutGlasses(); isLoad = false;
					for (int i = botIndex; i < topIndex; i++) switchGlassBitmap(i);
				}

				for (int i = botIndex; i < topIndex; i++) {
					glasses[i]->showGlassInfo(gamePlay.font, color.black, glasses[i]->getGlassPosition(), playerSelected);
				}

				if (playerSelected->getPlayerChance() > 10) manualGame(instruction);

				if (help.active) manualGame(rule);

				gameStatus();

				// keyboard key activate.
				key = true;
			}

			al_flip_display();
			al_clear_to_color(color.black);
		}
	}
}

void Game::destroy()
{
	al_destroy_display(display);
	al_destroy_event_queue(eventQueue);
	al_destroy_timer(timer);

	mainMenu.destroy();
	newGame.destroy();
	gamePlay.destroy();
	loadGame.destroy();
	leaderboard.destroy();
	leader.destroy();
	status.destroy();

	menu.destroy();
	load.destroy();
	play.destroy();
	back.destroy();
	reset.destroy();
	glassSelectedTop.destroy();
	glassSelectedBot.destroy();
	repeat.destroy();
	tap.destroy();
	save.destroy();
	home.destroy();

	backsound.destroy();
}
