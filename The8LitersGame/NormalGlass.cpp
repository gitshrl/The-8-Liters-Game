#include "NormalGlass.h"



NormalGlass::NormalGlass()
{
}


NormalGlass::~NormalGlass()
{
}

NormalGlass::NormalGlass(char glassPosition, int glassCapacity, float waterVolume, float oilVolume)
{
	this->glassPosition = glassPosition;
	this->glassCapacity = glassCapacity;
	this->waterVolume = waterVolume;
	this->oilVolume = oilVolume;
}

void NormalGlass::showGlassInfo(ALLEGRO_FONT *font, ALLEGRO_COLOR color, char position, Player *player)
{
	string txt1 = text.decimalPrecisionPoint(4, waterVolume);
	string txt2 = text.decimalPrecisionPoint(4, oilVolume);

	if (position == 'G') {
		al_draw_text(font, color, 272, 433, ALLEGRO_ALIGN_LEFT, txt1.c_str());
		al_draw_text(font, color, 272, 401, ALLEGRO_ALIGN_LEFT, txt2.c_str());
	}
	else if (position == 'B') {
		al_draw_text(font, color, 402, 260, ALLEGRO_ALIGN_LEFT, txt1.c_str());
		al_draw_text(font, color, 402, 229, ALLEGRO_ALIGN_LEFT, txt2.c_str());
	}
	else if (position == 'T') {
		al_draw_text(font, color, 310, 188, ALLEGRO_ALIGN_LEFT, txt1.c_str());
		al_draw_text(font, color, 310, 156, ALLEGRO_ALIGN_LEFT, txt2.c_str());
	}
}

void NormalGlass::saveGlassInfo()
{
	glassFile.open("glasses-saved.txt", ios::out | ios::app);

	glassFile << NORMAL_GLASS_TYPE << "\n" << glassPosition << " " << glassCapacity << " " << waterVolume << " " << oilVolume << endl;

	glassFile.close();
}
